const express = require('express');
const dataBase = require('./dataBase');
const posts = require('./app/posts');
const cors = require('cors');

const app = express();

const port = 8000;

app.use(cors());
app.use(express.json());
app.use(express.static('public'));
app.use('/posts', posts);

const run = async () => {
    await dataBase.init();

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
}

run().catch(e => console.log(e));


