const fs = require('fs').promises;
const {nanoid} = require("nanoid");

const dataPosts = './dataPosts.json';
let posts = [];

module.exports = {
    async addPost(post) {
        post.id = nanoid();
        posts.push(post);
        await this.save();
    },
    async save() {
        await fs.writeFile(dataPosts, JSON.stringify(posts, null, 2));
    },

    async init() {
        try {
            const fileData = await fs.readFile(dataPosts);
            posts = JSON.parse(fileData.toString());
        } catch (e) {
            posts = [];
        }
    },
    getPosts() {
        return posts;
    }


}