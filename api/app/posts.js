const express = require('express');
const router = express.Router();
const multer = require('multer');
const path = require('path');
const config = require('../config');
const {nanoid} = require('nanoid');
const dataBase = require('../dataBase');


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', (req, res) => {
    const posts = dataBase.getPosts();
    return res.send(posts);
});

router.post('/', upload.single('image'), async (req, res, next) => {
    try {
        if (!req.body.message) {
            return res.status(400).send({message: 'Empty message'});
        }
        const post = {
            name: req.body.name,
            message: req.body.message,
        };
        if (req.file) {
            post.image = req.file.filename;
        }
        await dataBase.addPost(post);
        const posts = dataBase.getPosts();
        return res.send(posts);
    } catch (e) {
        next(e);
    }


});

module.exports = router;