import { Component, Input, OnInit } from '@angular/core';
import { Post } from 'src/app/shared/post.model';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnInit {
  @Input() post!: Post;

  imageUrl!: string;

  constructor() {
  }

  ngOnInit(): void {
  }

  getImage(imageName: string) {
    return imageName ? this.imageUrl = environment.apiUrl + '/uploads/' + imageName : null;
  }

}
