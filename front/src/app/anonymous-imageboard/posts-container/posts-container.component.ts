import { Component, OnInit } from '@angular/core';
import { Post } from 'src/app/shared/post.model';
import { PostService } from 'src/app/shared/post.service';

@Component({
  selector: 'app-posts-container',
  templateUrl: './posts-container.component.html',
  styleUrls: ['./posts-container.component.sass']
})
export class PostsContainerComponent implements OnInit {
  posts: Post[] = [];

  constructor(private postService: PostService) {
  }

  ngOnInit(): void {
    this.postService.fetchPosts();
    this.postService.changePosts.subscribe(posts => this.posts = posts);
  }

}
