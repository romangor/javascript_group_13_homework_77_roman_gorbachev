import { Component, OnInit, ViewChild } from '@angular/core';
import { PostService } from 'src/app/shared/post.service';
import { NgForm } from '@angular/forms';
import { PostData } from '../../shared/post.model';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.sass']
})
export class CreatePostComponent implements OnInit {
  @ViewChild('f') form!: NgForm;

  constructor(private postService: PostService) {
  }

  ngOnInit(): void {
  }

  sendPost() {
    const post: PostData = this.form.value;
    this.postService.sendPost(post).subscribe(() => {
      this.postService.fetchPosts();
    });
  }

}
