import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PostsContainerComponent } from './anonymous-imageboard/posts-container/posts-container.component';
import { PostComponent } from './anonymous-imageboard/post/post.component';
import { CreatePostComponent } from './anonymous-imageboard/create-post/create-post.component';
import { FlexModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { FileInputComponent } from './ui/file-input/file-input.component';
import { FormsModule } from '@angular/forms';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { HttpClientModule } from '@angular/common/http';
import { NamePipe } from './pipes/name.pipe';

@NgModule({
  declarations: [
    AppComponent,
    PostsContainerComponent,
    PostComponent,
    CreatePostComponent,
    FileInputComponent,
    NamePipe
  ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FlexModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        MatButtonModule,
        FormsModule,
        ScrollingModule,
        HttpClientModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
