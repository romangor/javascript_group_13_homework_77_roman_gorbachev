export class Post {
  constructor(public id: string,
              public name: string,
              public message: string,
              public image: string) {
  }
}

export interface PostData {
  [key: string]: any;

  name: string;
  message: string;
  image: File | null;
}
