import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment as env } from 'src/environments/environment';
import { Post, PostData } from './post.model';
import { map, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  changePosts = new Subject<Post[]>();

  posts!: Post[];

  constructor(private http: HttpClient) {
  }

  sendPost(post: PostData) {
    const formData = new FormData();
    Object.keys(post).forEach(key => {
      formData.append(key, post[key]);
    });
    return this.http.post(env.apiUrl + '/posts', formData);
  };

  fetchPosts() {
    return this.http.get<{ [id: string]: Post }>('http://localhost:8000/posts')
      .pipe(map(result => {
        if (result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const data = result[id];
          return new Post(data.id, data.name, data.message, data.image);
        })
      }))
      .subscribe(posts => {
        this.posts = posts;
        this.changePosts.next(this.posts.slice().reverse());
      });
  }
}
